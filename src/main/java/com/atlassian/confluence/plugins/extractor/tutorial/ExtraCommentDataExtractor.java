package com.atlassian.confluence.plugins.extractor.tutorial;

import com.atlassian.confluence.core.ConfluenceEntityObject;
import com.atlassian.confluence.core.ContentEntityObject;
import com.atlassian.confluence.pages.Comment;
import com.atlassian.confluence.pages.CommentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.plugins.index.api.Extractor2;
import com.atlassian.confluence.plugins.index.api.FieldDescriptor;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.plugin.spring.scanner.annotation.imports.ComponentImport;
import com.google.common.collect.ImmutableList;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Collection;
import java.util.List;
import java.util.Objects;

import static java.util.Collections.emptyList;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.joining;

public class ExtraCommentDataExtractor implements Extractor2 {
    private final CommentManager commentManager;

    @Autowired
    public ExtraCommentDataExtractor(@ComponentImport CommentManager commentManager) {
        this.commentManager = requireNonNull(commentManager, "commentManager");
    }

    public StringBuilder extractText(Object searchable) {
        StringBuilder builder = new StringBuilder();
        if (searchable instanceof Page) {
            Page page = (Page) searchable;
            builder.append(commentManager.getPageComments(page.getId(), page.getCreationDate()).stream()
                    .map(ContentEntityObject::getBodyAsString)
                    .collect(joining(" ")));

        }
        return builder;
    }

    public Collection<FieldDescriptor> extractFields(Object searchable) {
        Page page = getPage(searchable);
        if (page == null) {
            return emptyList();
        }

        List<Comment> comments = commentManager.getPageComments(page.getId(), page.getCreationDate());
        if (comments.isEmpty()) {
            return emptyList();
        }

        ImmutableList.Builder<FieldDescriptor> builder = ImmutableList.builder();

        comments.stream()
                .map(ConfluenceEntityObject::getCreator)
                .filter(Objects::nonNull)
                .map(ConfluenceUser::getLowerName)
                .filter(Objects::nonNull)
                .forEach(username -> builder.add(ExtraCommentFields.CREATOR.createField(username)));

        Comment lastComment = comments.get(comments.size() - 1);
        builder.add(ExtraCommentFields.MODIFIED.createField(lastComment.getLastModificationDate()));
        builder.add(ExtraCommentFields.COUNT.createField(comments.size()));

        int commentTextLength = comments.stream()
                .mapToInt(x -> x.getBodyAsString().length())
                .sum();
        double commentScore = Math.log1p((double) commentTextLength / comments.size());
        builder.add(ExtraCommentFields.SCORE.createField(commentScore));

        return builder.build();
    }

    private Page getPage(Object searchable) {

        if (searchable instanceof Page) {
            return (Page) searchable;
        }

        if (searchable instanceof Comment) {
            Comment comment = (Comment) searchable;
            if (comment.getContainer() instanceof Page)
                return (Page) comment.getContainer();
        }

        return null;
    }
}
