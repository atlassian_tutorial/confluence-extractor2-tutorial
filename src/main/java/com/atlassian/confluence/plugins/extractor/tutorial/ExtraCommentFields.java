package com.atlassian.confluence.plugins.extractor.tutorial;

import com.atlassian.confluence.plugins.index.api.TwoGramAnalyzerDescriptor;
import com.atlassian.confluence.plugins.index.api.mapping.DateFieldMapping;
import com.atlassian.confluence.plugins.index.api.mapping.DoubleFieldMapping;
import com.atlassian.confluence.plugins.index.api.mapping.FieldMapping;
import com.atlassian.confluence.plugins.index.api.mapping.FieldMappingsProvider;
import com.atlassian.confluence.plugins.index.api.mapping.IntFieldMapping;
import com.atlassian.confluence.plugins.index.api.mapping.TextFieldMapping;

import java.util.Collection;

import static java.util.Arrays.asList;

public class ExtraCommentFields implements FieldMappingsProvider {

    public static final TextFieldMapping CREATOR = TextFieldMapping.builder("comment-creator").store(true).analyzer(new TwoGramAnalyzerDescriptor()).build();
    public static final DateFieldMapping MODIFIED = DateFieldMapping.builder("comment-modified").store(true).build();
    public static final IntFieldMapping COUNT = IntFieldMapping.builder("comment-count").store(true).build();
    public static final DoubleFieldMapping SCORE = DoubleFieldMapping.builder("comment-score").store(true).build();

    @Override
    public Collection<FieldMapping> getFieldMappings() {
        return asList(MODIFIED, COUNT, CREATOR, SCORE);
    }
}
